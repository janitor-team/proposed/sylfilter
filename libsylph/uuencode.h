/*
 * LibSylph -- E-Mail client library
 * Copyright (C) 1999-2011 Hiroyuki Yamamoto
 */

int touufrombits(unsigned char *, const unsigned char *, int);
int fromuutobits(char *, const char *);

#define X_UUENCODE_END_LINE '`'
#define UUENCODE_END_LINE ' '
