/*
 * LibSylph -- E-Mail client library
 * Copyright (C) 1999-2011 Hiroyuki Yamamoto
 */

#ifndef __BASE64_H__
#define __BASE64_H__

#include <glib.h>

typedef struct _Base64Decoder	Base64Decoder;

struct _Base64Decoder
{
	gint buf_len;
	gchar buf[4];
};

void base64_encode	(gchar		*out,
			 const guchar	*in,
			 gint		 inlen);
gint base64_decode	(guchar		*out,
			 const gchar	*in,
			 gint		 inlen);

Base64Decoder *base64_decoder_new	(void);
void	       base64_decoder_free	(Base64Decoder	*decoder);
gint	       base64_decoder_decode	(Base64Decoder	*decoder,
					 const gchar	*in,
					 guchar		*out);

#endif /* __BASE64_H__ */
