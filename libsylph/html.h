/*
 * LibSylph -- E-Mail client library
 * Copyright (C) 1999-2011 Hiroyuki Yamamoto
 */

#ifndef __HTML_H__
#define __HTML_H__

#include <glib.h>
#include <stdio.h>

#include "codeconv.h"

typedef enum
{
	HTML_NORMAL,
	HTML_PAR,
	HTML_BR,
	HTML_HR,
	HTML_HREF,
	HTML_IMG,
	HTML_FONT,
	HTML_PRE,
	HTML_UNKNOWN,
	HTML_CONV_FAILED,
	HTML_ERR,
	HTML_EOF
} HTMLState;

typedef struct _HTMLParser	HTMLParser;
typedef struct _HTMLAttr	HTMLAttr;
typedef struct _HTMLTag		HTMLTag;

struct _HTMLParser
{
	FILE *fp;
	CodeConverter *conv;

	GHashTable *symbol_table;

	GString *str;
	GString *buf;

	gchar *bufp;

	HTMLState state;

	gchar *href;

	gboolean newline;
	gboolean empty_line;
	gboolean space;
	gboolean pre;
};

struct _HTMLAttr
{
	gchar *name;
	gchar *value;
};

struct _HTMLTag
{
	gchar *name;
	GList *attr;
};

HTMLParser *html_parser_new	(FILE		*fp,
				 CodeConverter	*conv);
void html_parser_destroy	(HTMLParser	*parser);
const gchar *html_parse		(HTMLParser	*parser);

#endif /* __HTML_H__ */
