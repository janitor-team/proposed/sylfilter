Overview of Changes
===================

* 0.8 (2013/03/15)

    * Compilation error because of missing config.h inclusion was fixed.
    * Compilation error with newer GLib was fixed.

* 0.7 (2012/01/13)

    * External LibSylph (standalone or Sylpheed) can be used instead of
      built-in one.

* 0.6 (2011/11/14)

    * Received header is also used for classification now.
    * The verbose mode has several levels now.
    * Sylfilter command: an option to specify database directory was added.
    * Sylfilter command: -V (print version) option was added.

* 0.5 (2011/10/13)

    * The calculation of combined probability of Robinson-Fisher method was
      fixed. The version 0.4 wrongly applied the total number of words in
      a message without excluding the words within the minimum deviation.
      This fix will introduce more accurate filtering and a small speedup.
    * Sylfilter command: options to specify Robinson-Fisher parameters were
      added.
    * The bug that a multibyte filename could not be passed on Windows was
      fixed.

* 0.4 (2011/10/6)

    * Robinson-Fisher method was implemented, and it is used by default.
      This will improve the accuracy of filtering. Previous Paul/Naive bayes
      method still can be used with -m option.
    * Sylfilter command: an option to specify filtering method (-m) was added.

* 0.3 (2011/9/29)

    * Sylfilter command: -j and -C / -c and -J option can be specified
      at the same time
    * Sylfilter command: -B (no-bias for clean messages) option was added.
    * GDBM support was added (exclusive with QDBM)
    * APIs for global configuration were added

* 0.2 (2011/9/13)

    * Speedup learning (data incompatible to 0.1)
    * Support Windows

* 0.1 (2011/9/5)

    * Initial release
    * Supports QDBM and SQLite
    * Provides libsylfiter library and sylfilter command-line tool
    * Initial implementation of bayesian filter
