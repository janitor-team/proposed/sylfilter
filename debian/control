Source: sylfilter
Section: utils
Priority: optional
Maintainer: Hideki Yamane <henrich@debian.org>
Uploaders: HAYASHI Kentaro <hayashi@clear-code.com>,           
Build-Depends: debhelper (>= 12~), debhelper-compat (= 12),
               libglib2.0-dev, libsqlite3-dev, libqdbm-dev, libsylph-dev, gnulib
Standards-Version: 4.4.0
Homepage: https://sylpheed.sraoss.jp/sylfilter/
Vcs-Git: https://salsa.debian.org/sylpheed-team/sylfilter.git
Vcs-Browser: https://salsa.debian.org/sylpheed-team/sylfilter
Rules-Requires-Root: no

Package: sylfilter
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: generic spam filter tool
 SylFilter is a generic message filter library and command-line tools. It 
 provides a bayesian filter which is very popular as an algorithm for spam
 filtering. SylFilter also supports multilingual and can be applied to any
 languages. It is implemented with C language and runs fast  with small
 resources.
 .
  - Very lightweight and fast
  - Provides learning-type junk filter
  - Easy-to-use command line tool
  - Multilingual support (including CJKV)
  - Supports several embedded databases (SQLite and QDBM)

Package: libsylfilter0
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: generic spam filter library
 SylFilter is a generic message filter library and command-line tools. It 
 provides a bayesian filter which is very popular as an algorithm for spam
 filtering. SylFilter also supports multilingual and can be applied to any
 languages. It is implemented with C language and runs fast  with small
 resources.
 .
  - Very lightweight and fast
  - Provides learning-type junk filter
  - Multilingual support (including CJKV)
  - Simple and flexible library APIs
  - Supports several embedded databases (SQLite and QDBM)
