/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <locale.h>

#include "filter.h"
#include "filter-manager.h"
#include "filter-utils.h"
#include "filter-kvs.h"

#ifdef USE_QDBM
#  include "filter-kvs-qdbm.h"
#endif
#ifdef USE_SQLITE
#  include "filter-kvs-sqlite.h"
#endif
#ifdef USE_GDBM
#  include "filter-kvs-gdbm.h"
#endif

#include "textcontent-filter.h"
#include "blacklist-filter.h"
#include "whitelist-filter.h"
#include "wordsep-filter.h"
#include "bayes-filter.h"

enum {
	MODE_TEST_JUNK,
	MODE_LEARN,
	MODE_SHOW_STATUS
};

enum {
	MODE_LEARN_NONE = 0,
	MODE_LEARN_JUNK = 1,
	MODE_LEARN_CLEAN = 1 << 1,
	MODE_UNLEARN_JUNK = 1 << 2,
	MODE_UNLEARN_CLEAN = 1 << 3,
};

static int verbose = 0;

static int learn_filter(int mode, const char *file);
static int test_filter(int mode, const char *file);
static void print_message_data(XMessageData *msgdata);
static void version(void);
static void usage(void);


int main(int argc, char *argv[])
{
	int retval = 2;
	int i;
	int mode = MODE_TEST_JUNK;
	int learn_mode = MODE_LEARN_NONE;
	int no_bias = 0;
	const char *method = NULL;
	const char *min_dev = NULL;
	const char *robs = NULL;
	const char *robx = NULL;
	int count = 0;
	const char *dbpath = NULL;
#ifdef USE_QDBM
	const char *engine = "qdbm";
#elif defined(USE_SQLITE)
	const char *engine = "sqlite";
#elif defined(USE_GDBM)
	const char *engine = "gdbm";
#endif

	setlocale(LC_ALL, "");

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-j")) {
			mode = MODE_LEARN;
			learn_mode |= MODE_LEARN_JUNK;
		} else if (!strcmp(argv[i], "-c")) {
			mode = MODE_LEARN;
			learn_mode |= MODE_LEARN_CLEAN;
		} if (!strcmp(argv[i], "-J")) {
			mode = MODE_LEARN;
			learn_mode |= MODE_UNLEARN_JUNK;
		} else if (!strcmp(argv[i], "-C")) {
			mode = MODE_LEARN;
			learn_mode |= MODE_UNLEARN_CLEAN;
		} else if (!strcmp(argv[i], "-t"))
			mode = MODE_TEST_JUNK;
		else if (!strcmp(argv[i], "-s"))
			mode = MODE_SHOW_STATUS;
		else if (!strncmp(argv[i], "-v", 2)) {
			verbose = 1;
			if (argv[i][2] == 'v') {
				verbose++;
				if (argv[i][3] == 'v')
					verbose++;
			}
		} else if (!strcmp(argv[i], "-d"))
			xfilter_set_debug_mode(1);
		else if (!strcmp(argv[i], "-E")) {
			i++;
			if (i >= argc) {
				usage();
				return 1;
			}
			engine = argv[i];
		} else if (!strcmp(argv[i], "-B")) {
			no_bias = 1;
		} else if (!strcmp(argv[i], "-m")) {
			i++;
			if (i >= argc) {
				usage();
				return 1;
			}
			if (argv[i][0] == 'n')
				method = "n";
		} else if (!strcmp(argv[i], "-p")) {
			i++;
			if (i >= argc) {
				usage();
				return 1;
			}
			dbpath = argv[i];
		} else if (argv[i][0] == '-' && argv[i][1] == '-') {
			if (!strcmp(argv[i] + 2, "min-dev")) {
				i++;
				if (i >= argc) {
					usage();
					return 1;
				}
				min_dev = argv[i];
			} else if (!strcmp(argv[i] + 2, "robs")) {
				i++;
				if (i >= argc) {
					usage();
					return 1;
				}
				robs = argv[i];
			} else if (!strcmp(argv[i] + 2, "robx")) {
				i++;
				if (i >= argc) {
					usage();
					return 1;
				}
				robx = argv[i];
			} else if (!strncmp(argv[i] + 2, "help", 4)) {
				usage();
				return 0;
			}
		} else if (!strcmp(argv[i], "-h")) {
			usage();
			return 0;
		} else if (!strcmp(argv[i], "-V")) {
			version();
			return 0;
		}
	}

	xfilter_init(XF_APP_MODE_STANDALONE);

#ifdef USE_QDBM
	if (!strcasecmp(engine, "qdbm"))
		xfilter_kvs_qdbm_set_engine();
#else
	if (0) {}
#endif
#ifdef USE_SQLITE
	else if (!strcasecmp(engine, "sqlite"))
		xfilter_kvs_sqlite_set_engine();
#else
	else if (0) {}
#endif
#ifdef USE_GDBM
	else if (!strcasecmp(engine, "gdbm"))
		xfilter_kvs_gdbm_set_engine();
#else
	else if (0) {}
#endif
	else {
		fprintf(stderr, "Engine '%s' not supported.\n", engine);
		xfilter_done();
		return 127;
	}

	if (verbose)
		printf("engine %s has been selected\n", engine);

	/* set global config values */
	if (no_bias)
		xfilter_set_conf_value("no-bias", "t");
	if (method)
		xfilter_set_conf_value("method", method);
	if (min_dev)
		xfilter_set_conf_value("min-dev", min_dev);
	if (robs)
		xfilter_set_conf_value("robs", robs);
	if (robx)
		xfilter_set_conf_value("robx", robx);

	if (xfilter_utils_set_base_dir(dbpath) < 0) {
		fprintf(stderr, "Could not create base directory.\n");
		xfilter_done();
		return 127;
	}

	dbpath = xfilter_utils_get_base_dir();
	if (xfilter_bayes_db_init(dbpath) < 0) {
		fprintf(stderr, "Database initialization error.\n");
		xfilter_done();
		return 127;
	}

#define ARGV_OPTION_WITH_PARAM(arg)				\
	(!strcmp(arg, "-E") || !strcmp(arg, "-m") ||		\
	 !strcmp(arg, "-p") ||					\
	 !strcmp(arg, "--min-dev") || !strcmp(arg, "--robs") ||	\
	 !strcmp(arg, "--robx"))

	if (mode == MODE_SHOW_STATUS) {
		retval = xfilter_bayes_db_show_contents(verbose);
	} else if (mode == MODE_LEARN) {
		for (i = 1; i < argc; i++) {
			if (ARGV_OPTION_WITH_PARAM(argv[i])) {
				i++;
				if (i >= argc)
					break;
			} else if (argv[i][0] != '-') {
				retval = learn_filter(learn_mode, argv[i]);
				if (retval != 0)
					break;
				count++;
			}
		}
	} else {
		for (i = 1; i < argc; i++) {
			if (ARGV_OPTION_WITH_PARAM(argv[i])) {
				i++;
				if (i >= argc)
					break;
			} else if (argv[i][0] != '-') {
				retval = test_filter(mode, argv[i]);
				if (retval == 127)
					break;
				count++;
			}
		}
	}

	xfilter_bayes_db_done();
	xfilter_done();

	if (mode != MODE_SHOW_STATUS && count == 0)
		fprintf(stderr, "No input file.\n");

	if (verbose)
		printf("return value: %d\n", retval);

	return retval;
}

static int learn_filter(int mode, const char *file)
{
	XFilterManager *mgr;
	XMessageData *msgdata;
	XMessageData *resdata;
	XFilterResult *res;
	XFilterStatus status;
	int retval = 0;

	if (verbose)
		printf("learning message file: %s\n", file);

	if ((mode & (MODE_LEARN_JUNK | MODE_LEARN_CLEAN | MODE_UNLEARN_JUNK | MODE_UNLEARN_CLEAN)) == 0) {
		fprintf(stderr, "no learn mode specified\n");
		return 2;
	}
	if ((mode & (MODE_LEARN_JUNK | MODE_UNLEARN_CLEAN)) != 0 &&
	    (mode & (MODE_LEARN_CLEAN | MODE_UNLEARN_JUNK)) != 0) {
		fprintf(stderr, "-j/-C and -c/-J cannot be specified at the same time\n");
		return 2;
	}
	if ((mode & (MODE_LEARN_CLEAN | MODE_UNLEARN_JUNK)) != 0 &&
	    (mode & (MODE_LEARN_JUNK | MODE_UNLEARN_CLEAN)) != 0) {
		fprintf(stderr, "-c/-J and -j/-C cannot be specified at the same time\n");
		return 2;
	}

	mgr = xfilter_manager_new();
	xfilter_manager_filter_add(mgr, xfilter_textcontent_new());
	xfilter_manager_filter_add(mgr, xfilter_wordsep_new());

	if (mode & MODE_LEARN_JUNK)
		xfilter_manager_filter_add(mgr, xfilter_bayes_learn_junk_new());
	if (mode & MODE_LEARN_CLEAN)
		xfilter_manager_filter_add(mgr, xfilter_bayes_learn_nojunk_new());
	if (mode & MODE_UNLEARN_JUNK)
		xfilter_manager_filter_add(mgr, xfilter_bayes_unlearn_junk_new());
	if (mode & MODE_UNLEARN_CLEAN)
		xfilter_manager_filter_add(mgr, xfilter_bayes_unlearn_nojunk_new());

	msgdata = xfilter_message_data_read_file(file, "message/rfc822");

	res = xfilter_manager_run(mgr, msgdata);
	if (verbose)
		xfilter_result_print(res);
	status = xfilter_result_get_status(res);
	if (status == XF_UNSUPPORTED_TYPE || status == XF_ERROR) {
		fprintf(stderr, "%s: Error on learning mail\n", file);
		retval = 127;
	}

	if (xfilter_get_debug_mode()) {
		resdata = xfilter_result_get_message_data(res);
		print_message_data(resdata);
	}

	xfilter_result_free(res);
	xfilter_message_data_free(msgdata);
	xfilter_manager_free(mgr);

	return retval;
}

static int test_filter(int mode, const char *file)
{
	XFilterManager *mgr;
	XMessageData *msgdata;
	XMessageData *resdata;
	XFilterResult *res;
	XFilterStatus status;
	int retval = 0;

	XFilterConstructorFunc ctors[] = {
		xfilter_textcontent_new,
		xfilter_blacklist_new,
		xfilter_whitelist_new,
		xfilter_wordsep_new,
		xfilter_bayes_new,
		NULL
	};

	if (verbose)
		printf("testing message file: %s\n", file);

	mgr = xfilter_manager_new();
	xfilter_manager_add_filters(mgr, ctors);

	msgdata = xfilter_message_data_read_file(file, "message/rfc822");

	res = xfilter_manager_run(mgr, msgdata);
	if (verbose)
		xfilter_result_print(res);
	status = xfilter_result_get_status(res);
	if (status == XF_JUNK) {
		printf("%s: This is a junk mail (prob: %f)\n", file, xfilter_result_get_probability(res));
		retval = 0;
	} else if (status == XF_UNCERTAIN) {
		printf("%s: This mail could not be classified (prob: %f)\n", file, xfilter_result_get_probability(res));
		retval = 2;
	} else if (status == XF_UNSUPPORTED_TYPE || status == XF_ERROR) {
		printf("%s: Error on testing mail\n", file);
		retval = 127;
	} else {
		printf("%s: This is a clean mail (prob: %f)\n", file, xfilter_result_get_probability(res));
		retval = 1;
	}

	if (xfilter_get_debug_mode()) {
		resdata = xfilter_result_get_message_data(res);
		print_message_data(resdata);
	}

	xfilter_result_free(res);
	xfilter_message_data_free(msgdata);

	xfilter_manager_free(mgr);

	return retval;
}

static void print_message_data(XMessageData *msgdata)
{
	const char *content;

	if (!msgdata)
		return;

	printf("\n");

	content = xfilter_message_data_get_attribute(msgdata, XM_FROM);
	if (content)
		printf("from: %s\n", content);
	content = xfilter_message_data_get_attribute(msgdata, XM_TO);
	if (content)
		printf("to: %s\n", content);
	content = xfilter_message_data_get_attribute(msgdata, XM_CC);
	if (content)
		printf("cc: %s\n", content);
	content = xfilter_message_data_get_attribute(msgdata, XM_SUBJECT);
	if (content)
		printf("subject: %s\n", content);
	content = xfilter_message_data_get_attribute(msgdata, XM_RECEIVED);
	if (content)
		printf("received: %s\n", content);

	if (verbose > 2) {
		content = xfilter_message_data_get_content(msgdata);
		printf("content: %s\n", content);
	}
}

static void version(void)
{
	printf("SylFilter (tentative name) version " VERSION "\n");
}

static void usage(void)
{
	version();
	printf("\n");
	printf("Usage: sylfilter [options] message [message ...]\n");
	printf("\n");
	printf("Options:\n");
	printf("  -j  learn junk (spam) messages\n");
	printf("  -c  learn clean (non-spam) messages\n");
	printf("  -J  unlearn junk (spam) messages\n");
	printf("  -C  unlearn clean (non-spam) messages\n");
	printf("  -t  classify messages\n");
	printf("  -v  show verbose messages\n");
	printf("  -d  show debug messages\n");
	printf("  -m n|r\n");
	printf("      specify filtering method\n");
	printf("      n : Paul Graham (Naive Bayes) method\n");
	printf("      r : Gary Robinson (Robinson-Fisher) method (default)\n");
	printf("  --min-dev\n");
	printf("      ignore if score near (default: 0.1)\n");
	printf("  --robs\n");
	printf("      Robinson's s parameter (default: 1.0)\n");
	printf("  --robx\n");
	printf("      Robinson's x parameter (default: 0.5)\n");
	printf("  -B  do not bias probability for clean mail\n");
	printf("      (Paul/Naive method only, may increase false-positive)\n");
	printf("\n");
	printf("  -V  print version\n");
	printf("  -h, --help\n");
	printf("      print this help message\n");
	printf("\n");
	printf("  -E <engine_name>\n");
	printf("      specify key-value store engine (show below)\n");
	printf("  -p <path>\n");
	printf("      specify database directory\n");
	printf("\n");
	printf("Return values:\n");
	printf("  0   junk (spam)\n");
	printf("  1   clean (non-spam)\n");
	printf("  2   uncertain\n");
	printf("  127 other errors\n");
	printf("\n");
	printf("Default database location: %s/*.db\n",
	       xfilter_utils_get_default_base_dir());
	printf("\n");
	printf("Available key-value stores:\n");
#ifdef USE_QDBM
	printf("  QDBM\n");
#endif
#ifdef USE_SQLITE
	printf("  SQLite\n");
#endif
#ifdef USE_GDBM
	printf("  GDBM\n");
#endif
}
