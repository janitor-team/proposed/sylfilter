/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_KVS_GDBM__
#define __FILTER_KVS_GDBM__

int xfilter_kvs_gdbm_set_engine(void);

#endif /* __FILTER_KVS_GDBM__ */
