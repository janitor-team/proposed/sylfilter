/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_UTILS_H__
#define __FILTER_UTILS_H__

char	*xfilter_utils_get_file_contents(const char *file);
void	 xfilter_free_mem(void *mem);

const char *xfilter_utils_get_home_dir(void);
const char *xfilter_utils_get_base_dir(void);
int	 xfilter_utils_set_base_dir(const char *path);
const char *xfilter_utils_get_default_base_dir(void);

int	 xfilter_utils_mkdir(const char *path);

void	 xfilter_set_conf_value(const char *key, const char *value);
const char *xfilter_get_conf_value(const char *key);
void xfilter_conf_value_clear(void);

#endif /* __FILTER_UTILS_H__ */
