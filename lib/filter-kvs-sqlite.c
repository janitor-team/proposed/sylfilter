/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include "config.h"

#ifdef USE_SQLITE

#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

#include "filter-kvs.h"
#include "filter-kvs-sqlite.h"

static XFilterKVS *sqlite_open(const char *dbfile);
static int sqlite_close(XFilterKVS *kvs);
static int sqlite_insert(XFilterKVS *kvs, const char *key, void *value, int size);
static int sqlite_delete(XFilterKVS *kvs, const char *key);
static int sqlite_update(XFilterKVS *kvs, const char *key, void *value, int size);
static int sqlite_fetch(XFilterKVS *kvs, const char *key, void *vbuf, int vsize);
static int sqlite_begin(XFilterKVS *kvs);
static int sqlite_end(XFilterKVS *kvs);
static int sqlite_size(XFilterKVS *kvs);
static int sqlite_foreach(XFilterKVS *kvs, XFilterKVSForeachFunc func, void *data);


int xfilter_kvs_sqlite_set_engine(void)
{
	XFilterKVSEngine engine = {
		sqlite_open,
		sqlite_close,
		sqlite_insert,
		sqlite_delete,
		sqlite_update,
		sqlite_fetch,
		sqlite_begin,
		sqlite_end,
		sqlite_size,
		sqlite_foreach
	};

	return xfilter_kvs_set_engine(&engine);
}

static XFilterKVS *sqlite_open(const char *dbfile)
{
	sqlite3 *dbhandle = NULL;
	int ret;

	if (sqlite3_open(dbfile, &dbhandle) != SQLITE_OK) {
		sqlite3_close(dbhandle);
		return NULL;
	}
	if ((ret = sqlite3_exec(dbhandle, "CREATE TABLE kvs (key TEXT PRIMARY KEY, value INTEGER NOT NULL)", NULL, NULL, NULL)) != SQLITE_OK) {
		if (ret != SQLITE_ERROR) {
			fprintf(stderr, "sqlite_open: sqlite3_exec: returned %d\n", ret);
			sqlite3_close(dbhandle);
			return NULL;
		}
	}
	return xfilter_kvs_new(dbfile, (void *)dbhandle);
}

static int sqlite_close(XFilterKVS *kvs)
{
	if (sqlite3_close((sqlite3 *)xfilter_kvs_get_handle(kvs)) != SQLITE_OK)
		return -1;
	return 0;
}

static int sqlite_insert(XFilterKVS *kvs, const char *key, void *value, int size)
{
	sqlite3 *db;
	char buf[1024];
	int ival;
	int ret;

	if (size != 4)
		return -1;

	ival = *((gint32 *)value);

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	sqlite3_snprintf(sizeof(buf), buf,
			 "INSERT INTO kvs VALUES ('%q', %d)", key, ival);
	if ((ret = sqlite3_exec(db, buf, NULL, NULL, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: insert: returned %d\n", ret);
		return -1;
	}
	return 0;
}

static int sqlite_delete(XFilterKVS *kvs, const char *key)
{
	sqlite3 *db;
	char buf[1024];
	int ret;

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	sqlite3_snprintf(sizeof(buf), buf,
			 "DELETE FROM kvs WHERE key = '%q'", key);
	if ((ret = sqlite3_exec(db, buf, NULL, NULL, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: delete: returned %d\n", ret);
		return -1;
	}
	return 0;
}

static int sqlite_update(XFilterKVS *kvs, const char *key, void *value, int size)
{
	sqlite3 *db;
	char buf[1024];
	int ival;
	int ret;

	if (size != 4)
		return -1;

	ival = *((gint32 *)value);

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	sqlite3_snprintf(sizeof(buf), buf,
			 "UPDATE kvs SET value = %d WHERE key = '%q'", ival, key);
	if ((ret = sqlite3_exec(db, buf, NULL, NULL, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: update: returned %d\n", ret);
		return -1;
	}
	return 0;
}

static int fetch_cb(void *data, int count, char **cols, char **names)
{
	char *val;

	val = cols[0];
	*((gint32 *)data) = atoi(val);
	return 0;
}

static int sqlite_fetch(XFilterKVS *kvs, const char *key, void *vbuf, int vsize)
{
	sqlite3 *db;
	char buf[1024];
	gint32 ival = -1;
	int ret;

	if (vsize != 4)
		return -1;

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	sqlite3_snprintf(sizeof(buf), buf,
			 "SELECT value FROM kvs WHERE key = '%q'", key);
	if ((ret = sqlite3_exec(db, buf, fetch_cb, &ival, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: update: returned %d\n", ret);
		return -1;
	}

	if (ival == -1)
		return -1;

	*((gint32 *)vbuf) = ival;

	return sizeof(ival);
}

static int sqlite_begin(XFilterKVS *kvs)
{
	sqlite3 *db;
	int ret;

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	if ((ret = sqlite3_exec(db, "BEGIN", NULL, NULL, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: begin: returned %d\n", ret);
		return -1;
	}

	return 0;
}

static int sqlite_end(XFilterKVS *kvs)
{
	sqlite3 *db;
	int ret;

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	if ((ret = sqlite3_exec(db, "COMMIT", NULL, NULL, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: commit: returned %d\n", ret);
		return -1;
	}

	return 0;
}

static int size_cb(void *data, int count, char **cols, char **names)
{
	char *val;

	val = cols[0];
	*((int *)data) = atoi(val);
	return 0;
}

static int sqlite_size(XFilterKVS *kvs)
{
	sqlite3 *db;
	char buf[1024];
	int ival = 0;
	int ret;

	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	sqlite3_snprintf(sizeof(buf), buf, "SELECT count(key) FROM kvs");
	if ((ret = sqlite3_exec(db, buf, size_cb, &ival, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: update: returned %d\n", ret);
		return -1;
	}

	return ival;
}

struct cbdata
{
	XFilterKVS *kvs;
	XFilterKVSForeachFunc func;
	void *data;
};

static int foreach_cb(void *data, int count, char **cols, char **names)
{
	const char *key;
	const char *val;
	gint32 ival;
	int r;
	struct cbdata *cbdata;

	cbdata = (struct cbdata *)data;
	key = cols[0];
	val = cols[1];
	ival = atoi(val);
	r = cbdata->func(cbdata->kvs, key, &ival, sizeof(ival), cbdata->data);
	if (r < 0)
		return -1;
	return 0;
}

static int sqlite_foreach(XFilterKVS *kvs, XFilterKVSForeachFunc func, void *data)
{
	sqlite3 *db;
	char buf[1024];
	int ret;
	struct cbdata cbdata;

	cbdata.kvs = kvs;
	cbdata.func = func;
	cbdata.data = data;
	db = (sqlite3 *)xfilter_kvs_get_handle(kvs);
	sqlite3_snprintf(sizeof(buf), buf, "SELECT key, value FROM kvs");
	if ((ret = sqlite3_exec(db, buf, foreach_cb, &cbdata, NULL)) != SQLITE_OK) {
		fprintf(stderr, "sqlite3_exec: update: returned %d\n", ret);
		if (ret != SQLITE_ABORT)
			return -1;
	}

	return 0;
}

#endif /* USE_SQLITE */
