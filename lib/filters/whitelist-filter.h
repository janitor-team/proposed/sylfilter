/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __XFILTER_WHITELIST_H__
#define __XFILTER_WHITELIST_H__

#include "filter.h"

XFilter *xfilter_whitelist_new(void);

#endif /* __XFILTER_WHITELIST_H__ */
