/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include <glib.h>

#include "filter.h"
#include "ngram-filter.h"


static char *do_ngram(const char *content)
{
	return g_strdup(content);
}

static XFilterStatus xfilter_ngram_func(XFilter *filter, const XMessageData *data, XFilterResult *result)
{
	const char *mime_type;
	const char *content = NULL;
	char *processed_content;
	XMessageData *msgdata;

	g_return_val_if_fail(result != NULL, XF_ERROR);

	mime_type = xfilter_message_data_get_mime_type(data);
	if (!mime_type) {
		xfilter_result_set_status(result, XF_UNSUPPORTED_TYPE);
		return XF_UNSUPPORTED_TYPE;
	}

	if (!g_strncasecmp(mime_type, "text/", 5))
		content = xfilter_message_data_get_content(data);
	else {
		xfilter_result_set_status(result, XF_UNSUPPORTED_TYPE);
		return XF_UNSUPPORTED_TYPE;
	}

	processed_content = do_ngram(content);

	msgdata = xfilter_message_data_new(NULL, mime_type);
	xfilter_message_data_set_content(msgdata, processed_content);
	xfilter_message_data_copy_attributes(msgdata, data);
	xfilter_result_set_message_data(result, msgdata);

	xfilter_result_set_status(result, XF_REWRITTEN);

	return XF_REWRITTEN;
}

XFilter *xfilter_ngram_new(void)
{
	XFilter *filter;

	filter = xfilter_new(XF_CONTENT, "ngram");
	xfilter_set_content_filter_func(X_CONTENT_FILTER(filter),
					xfilter_ngram_func);

	return filter;
}
