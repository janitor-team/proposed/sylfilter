/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __XFILTER_WORDSEP_H__
#define __XFILTER_WORDSEP_H__

#include "filter.h"

XFilter *xfilter_wordsep_new(void);

#endif /* __XFILTER_WORDSEP_H__ */
