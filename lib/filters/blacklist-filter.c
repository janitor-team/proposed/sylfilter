/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include <glib.h>

#include "filter.h"
#include "blacklist-filter.h"


static XFilterStatus xfilter_blacklist_func(XFilter *filter, const XMessageData *data, XFilterResult *result)
{
	return XF_NOJUNK;
}

XFilter *xfilter_blacklist_new(void)
{
	XFilter *filter;

	filter = xfilter_new(XF_TEST, "blacklist");
	xfilter_set_test_filter_func(X_TEST_FILTER(filter),
				     xfilter_blacklist_func);

	return filter;
}
