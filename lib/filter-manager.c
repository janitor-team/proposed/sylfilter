/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include <glib.h>

#include "filter.h"
#include "filter-manager.h"
#include "filter-private.h"


XFilterManager *xfilter_manager_new(void)
{
	XFilterManager *mgr;

	mgr = g_new0(XFilterManager, 1);
	return mgr;
}

void xfilter_manager_free(XFilterManager *mgr)
{
	XFilter *list;
	XFilter *next;

	if (!mgr)
		return;

	for (list = mgr->filter_list; list != NULL; list = next) {
		next = list->next;
		xfilter_free(list);
	}
	g_free(mgr->original_encoding);

	g_free(mgr);
}

void xfilter_manager_filter_add(XFilterManager *mgr, XFilter *filter)
{
	XFilter *list;

	g_return_if_fail(mgr != NULL);
	g_return_if_fail(filter != NULL);

	list = mgr->filter_list;
	if (list) {
		while (list->next) {
			list = list->next;
		}
		list->next = filter;
	} else {
		mgr->filter_list = filter;
	}

	filter->manager = mgr;
}

void xfilter_manager_filter_remove(XFilterManager *mgr, XFilter *filter)
{
	XFilter *list;
	XFilter *prev = NULL;

	g_return_if_fail(mgr != NULL);
	g_return_if_fail(filter != NULL);

	for (list = mgr->filter_list; list != NULL; list = list->next) {
		if (list == filter) {
			if (prev)
				prev->next = list->next;
			list->next = NULL;
			filter->manager = NULL;
			break;
		}
		prev = list;
	}
}

int xfilter_manager_add_filters(XFilterManager *mgr, XFilterConstructorFunc ctors[])
{
	XFilter *filter;
	int i;

	for (i = 0; ctors[i] != NULL; i++) {
		filter = ctors[i]();
		if (!filter)
			return -1;
		xfilter_manager_filter_add(mgr, filter);
	}

	return 0;
}

void xfilter_manager_done(XFilterManager *mgr)
{
	g_return_if_fail(mgr != NULL);

	g_free(mgr->original_encoding);
	mgr->original_encoding = NULL;
}

XFilterResult *xfilter_manager_run(XFilterManager *mgr, XMessageData *msgdata)
{
	XFilter *list;
	const XMessageData *curdata;
	XFilterResult *res;
	XFilterStatus status;

	g_return_val_if_fail(mgr != NULL, NULL);

	xfilter_debug_print("%s: %s: run filter chain\n", __FILE__, G_STRFUNC);

	res = xfilter_result_new();

	curdata = msgdata;

	for (list = mgr->filter_list; list != NULL; list = list->next) {
		xfilter_debug_print("exec filter: %s [%s] in data type: %s\n", xfilter_get_name(list), xfilter_get_type(list) == XF_CONTENT ? "content filter" : "test filter", xfilter_message_data_get_mime_type(curdata));

		status = xfilter_exec(list, curdata, res);
		if (res->msgdata)
			curdata = res->msgdata;

		xfilter_debug_print("exec filter: %s: status %d: out data type: %s\n", xfilter_get_name(list), status, xfilter_message_data_get_mime_type(curdata));

		if (status == XF_JUNK) {
			xfilter_debug_print("filter returned XF_JUNK, end filter chain\n");
			break;
		}
		if (status == XF_UNSUPPORTED_TYPE || status == XF_ERROR) {
			xfilter_debug_print("filter returned error, end filter chain\n");
			break;
		}
	}

	xfilter_manager_done(mgr);

	return res;
}
